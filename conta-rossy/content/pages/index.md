---
title: Home
sections:
  - section_id: hero
    type: section_hero
    title: RYG SERVICIOS CONTABLES.
    image: images/5.jpg
    content: >-
      De manera legal, responsable y puntual.
    actions:
      - label: Contáctanos
        url: /docs
        style: primary
  - section_id: features
    type: section_grid
    col_number: three
    grid_items:
      - title: ASESORIAS ADMINISTRATIVAS
        content: >-
          * Asesoria en creacion y constitucion de sociedades 

          * Asesoria en temas de nomina y talento humano 

          * Asesoria en temas documentales 
        actions:
          - label: Solicitar Asesoria
            url: /docs
            style: link
      - title: TEMAS CONTABLES
        content: >-
          * Contabilidades para personas naturales y juridicas 

          * Elaboracion de presupuestos 

          * Elaboracion de estados financieros 

          * Liquidacion de impuestos nacionales , distritales y municipales. 

          * Asesoria en tramites ante las entidades de impuestos nacionales, distritales y municipales. 
        actions:
          - label: Solicitar Servicio
            url: /blog
            style: link
      - title: PRESENTACION DE REPORTES E INFORMES
        content: >-
          * Asesoria en la presentacion de informes y reportes a entidades de vigilancia y control (DANE, SUPERINTENDENCIAS). 

          * Asesoria y presentacion de informacion exogena a nivel nacional distrital y municipal.  
        actions:
          - label: Ayuda en el tramite
            url: /style-guide
            style: link
  - section_id: text-img
    type: section_content
    image: images/jamstack.svg
    image_position: left
    title: Servicios de AUDITORIA Y REVISORIA FISCAL 
    content: >-
      Nam pulvinar ante eu ultricies volutpat. Sed nulla nibh, dapibus sit amet
      cursus quis, fringilla nec sapien. Vestibulum imperdiet nunc bibendum
      consectetur lobortis.
    actions:
      - label: Contactar
        url: /docs/getting-started
        style: primary
  - section_id: cta
    type: section_cta
    title: Si necesitas ayuda no dudes en contactarnos
    subtitle: Puedes hacerlo por el formulario de solicitud de la pagina.
    actions:
      - label: Solicitar
        url: /docs/getting-started/installation
        style: primary
seo:
  title: RyG Servicios Contables
  description: Servicios Contables
  extra:
    - name: 'og:type'
      value: website
      keyName: property
    - name: 'og:title'
      value: Stackbit Libris Theme
      keyName: property
    - name: 'og:description'
      value: The preview of the Libris theme
      keyName: property
    - name: 'og:image'
      value: images/4.jpg
      keyName: property
      relativeUrl: true
    - name: 'twitter:card'
      value: summary_large_image
    - name: 'twitter:title'
      value: Stackbit Libris Theme
    - name: 'twitter:description'
      value: The preview of the Libris theme
    - name: 'twitter:image'
      value: images/4.jpg
      relativeUrl: true
layout: advanced
---
