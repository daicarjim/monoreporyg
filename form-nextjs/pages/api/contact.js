export default async (req, res) => {

const nodemailer = require("nodemailer");

  //const { name, email, phone, message } = JSON.parse(req.body);

require('dotenv').config()

  const transporter = nodemailer.createTransport({
    port: 465,
    host: "smtp.gmail.com",
    auth: {
        user: 'apicorreosryg@gmail.com',
        pass: process.env.password,
    },
    secure: true,
});

/*await new Promise((resolve, reject) => {
    // verify connection configuration
    transporter.verify(function (error, success) {
        if (error) {
            console.log(error);
            reject(error);
        } else {
            console.log("Server is ready to take our messages");
            resolve(success);
        }
    });
});
*/
const mailData = {
    from: 'apicorreosryg@gmail.com',
    to: 'rygservicioscontables@gmail.com',
    subject: `Message From ${req.body.name}`,
    text: req.body.message + " | Sent from: " + req.body.email,
    html: `<div>${req.body.message}</div><p>Sent from:
    ${req.body.email}</p><p>Phone contact: ${req.body.phone}</p>`

};

await new Promise((resolve, reject) => {
    // send mail
    transporter.sendMail(mailData, (err, info) => {
        if (err) {
            console.error(err);
            reject(err);
        } else {
            console.log(info);
            resolve(info);
        }
    });
});
res.status(200).json({ status: "OK" })
};
