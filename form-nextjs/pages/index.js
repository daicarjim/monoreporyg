import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import { useState } from 'react'
import swal from 'sweetalert';



export default function Home() {

const [name, setName] = useState('')
const [email, setEmail] = useState('')
const [phone, setPhone] = useState('')
const [message, setMessage] = useState('')
const [submitted, setSubmitted] = useState(false)

  const handleSubmit = (e) => {

     e.preventDefault()


    if (phone.length == 0 || name.length == 0 || email.length == 0 || message.length == 0){
           swal("No Send!", "Please fill all items", "error")
           return false;
   }
     else{

     swal("Send Success!", "Thanks for contact us", "success")
    console.log('Sending')

    function reload() {
    document.location.reload();
    }
  setTimeout(reload, 5000);

   }

    let data = {
        name,
        email,
        phone,
        message
    }
    fetch('/api/contact', {
      method: 'POST',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then((res) => {
        console.log('Response received')
        if (res.status === 200) {
            console.log('Response succeeded!')
            setSubmitted(true) 
            setName('')
            setEmail('')
            setPhone('')
            setMessage('')
        }
    })
  }


  return (
<div className={styles.container}>
  <div className={styles.title}>
    CONTACT US
  </div>  
  < form className={styles.main} >
  < formGroup className={styles.inputGroup} >
    < label htmlFor='name'>Name</label>
    < input type='text' onChange={(e)=>{setName(e.target.value)}} name='name' className={styles.inputField} />
  </formGroup>
  < formGroup className={styles.inputGroup} >
    < label htmlFor='email'>Email</label>
    < input type='email' onChange={(e)=>{setEmail(e.target.value)}} email='email' className={styles.inputField} />
  </formGroup>
  < formGroup className={styles.inputGroup} >
    < label htmlFor='phone'>Phone</label>
    < input type='phone' onChange={(e)=>{setPhone(e.target.value)}} phone='phone' className={styles.inputField} />
  </formGroup>
  < formGroup className={styles.inputGroup} >
    < label htmlFor='message'>Message</label>
    < textarea type='text' onChange={(e)=>{setMessage(e.target.value)}} message='message' className={styles.inputMessage} />
  </formGroup>
  </form >
    <div className={styles.footerButton1}>
      < input type='submit' className={styles.inputButton1} onClick={(e)=>{handleSubmit(e)}} />
    </div>
    <div className={styles.footerButton2}>
       <button className={styles.inputButton2}onClick={() => window.location = "http://www.rygservicioscontables.ml"}>Back Home</button>
    </div>

</div>
   )

}
